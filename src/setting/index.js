import React, { Component } from 'react';
import { View, YellowBox, Text, Image, StatusBar, TouchableOpacity, StyleSheet, TextInput, Picker} from 'react-native';
import { Header, Footer, FooterTab, Container, Content, Right, Left, Body, Title, Button, Icon, Tab, Tabs, TabHeading, Card, CardItem, List, ListItem, Form, Item, Label, Input } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';


export default class LeaveRequest extends Component
{

    constructor(props)
    {
        super(props);

        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: isMounted is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);

        console.disableYellowBox = true;
    }


    state = 
    {
        PickerValueHolder : '',
    };



    static navigationOptions = 
    {
        tabBarLabel: 'Setting',
        // Note: By default the icon is only shown on iOS. Search the showIcon option below.
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require('../../assets/icon/active-setting.png')}
            style={{width:40, height:40}}
          />
        ),
        tabBarIcon: ({ focused }) => (
            focused 
            ? <Image source={require('../../assets/icon/active-setting.png')} style={{width:40, height:40}} />
            : <Image source={require('../../assets/icon/setting.png')} style={{width:40, height:40}} />
          ),
          
      };


    render()
    {
        return(
            <Container style={{flex:1}}>
                <Header androidStatusBarColor="#026296" style={{backgroundColor:"#026296", elevation:0}}> 
                    <Body>
                        <Title>Setting</Title>
                    </Body>
                </Header>
                <Content>
                    <View style={{flex:1, flexDirection:'column'}}>
                        <View style={{marginLeft:20, marginRight:20, marginTop:20}}>
                            <Text style={{}}>Language</Text>
                            <Picker
                                color = "#212121"
                                selectedValue={this.state.PickerValueHolder}
                                onValueChange={(itemValue, itemIndex) => this.setState({PickerValueHolder: itemValue})} 
                                style={{borderBottomColor:'#212121', borderBottomWidth:1}}>
                                {/* <Item label="blue" color="blue" value="blue" /> */}
                                <Picker.Item label="Choose Language" value="" />
                                <Picker.Item label="English" value="1" />
                                <Picker.Item label="Indonesia" value="2" />
                            </Picker>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}
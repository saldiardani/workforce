import React, { Component } from 'react';
import { View, YellowBox, Text, Image, StatusBar, StyleSheet, ListView, FlatList, ActivityIndicator, AsyncStorage} from 'react-native';
import { Header, Footer, FooterTab, Container, Content, Right, Left, Body, Title, Button, Icon, Tab, Tabs, TabHeading, Card, CardItem, List, ListItem } from 'native-base';
import { Divider, Avatar } from 'react-native-elements';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import Storage from 'react-native-storage';

import Utility from '../../config/utility';
import Network from '../../config/network';

let randomColor = Utility.getRandomColor();
let employeeID = '';
let currentDate = '';
let username = '';
let jobtitle = '';
let dateAPI  = '';

let storage = new Storage({
    size: 1000,
    storageBackend: AsyncStorage,
    defaultExpires: 1000 * 3600 * 24,
    enableCache: true,
    sync : {}
});


export default class TaskList extends Component
{
    constructor(props)
    {
        super(props);

        currentDate_temp = Utility.getCurrentDate();

        let day    = (currentDate_temp[0] < 10) ? '0' + currentDate_temp[0] : currentDate_temp[0];
        let month  = (currentDate_temp[1] < 10) ? '0' + currentDate_temp[1] : currentDate_temp[1];

        dateAPI = currentDate_temp[2] + '-' + month + '-' + day

    }

    state=
    {
        isloading       : true,
        dataSource      : [],
        isfetching      : false
    }

    fetchData = () =>
    {
        this.setState({isfetching : true});

        storage.load
        ({
            key: 'loginState',
            autoSync: true,
            syncInBackground: true,
            syncParams: 
            {
              extraFetchOptions: 
              {
                // blahblah
              },
              someFlag: true,
            },
        })
        
        .then(ret => 
        {
                console.log(ret.userid);
                console.log(ret.username);
                console.log(ret.token);
        
                username = ret.username;
                jobtitle = ret.roleDesc;
                let userid = ret.userid;
                let token = ret.token;  
                
                //EndPoint = 'http://172.25.230.165/dispatch/task-list/list/' + userid;
                //EndPoint = 'http://ads-api.asyst.co.id/dispatch/task-list/list/' + userid;

                EndPoint = 'http://ads-api.asyst.co.id/dispatch/task-list/list-all/' + userid + '/' + dateAPI;

                console.log(EndPoint);

                Network.reqdatalistdashboard(EndPoint)
                .then( (response) => 
                {
                    this.setState({
                        isLoading   : false,
                        dataSource  : response,
                        isfetching  : false
                    });

                    console.log(this.state.dataSource);
                });
        })
        
        .catch(err => 
        {
                console.warn(err.message);
                switch (err.name) 
                {
                    case 'NotFoundError':
                        // TODO;
                        break;
                    case 'ExpiredError':
                        // TODO
                        break;
                }
        });
    }

    componentDidMount()
    {
        this.fetchData();
    }

    static navigationOptions = 
    {
        tabBarLabel: 'Task List',
        // Note: By default the icon is only shown on iOS. Search the showIcon option below.
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require('../../assets/icon/active-taskList.png')}
            style={{width:40, height:40}}
          />
        ),
        tabBarIcon: ({ focused }) => (
            focused 
            ? <Image source={require('../../assets/icon/active-taskList.png')} style={{width:40, height:40}} />
            : <Image source={require('../../assets/icon/taskList.png')} style={{width:40, height:40}} />
          )
      };


      listPressed = (id) => 
      {
          this.setState
          (
              {
                  id_list : id 
              }, 
              ()=>
              {
                  this.setState({ id_list: id });
                  console.log('id_list : '+this.state.id_list);

                  this.props.navigation.navigate('TaskListDetail', {idlist : this.state.id_list });
              }
          );
      }

     
      renderItem = ({ item }) => 
      (
        
        <Button style={{ backgroundColor:'#fff', height:70, marginLeft:20, marginRight:20, marginTop:6, marginBottom:6, borderRadius:10, backgroundColor:'#fff', borderLeftWidth:7, borderColor:"#026296" }} onPress={this.listPressed.bind(this,item.id)} transparent>
            <View style={{ flex:10, flexDirection:'column' }} >
                <Text style={{ fontSize:20, paddingLeft:20, paddingTop:10, color:'#026296' }}> {item.qualificationCode} </Text>

                <View style={{ flex:0.8, flexDirection:'row', paddingLeft:16, marginTop:8, alignContent:'center', alignItems:'center' }}>
                    <Image source={require('../../assets/icon/active-time.png')} style={{ width:30, height:30 }}/>

                    <Text style={{ fontSize:15 }}>
                        { Utility.getHourMinute(item.startTime) + ' - '+ Utility.getHourMinute(item.endTime)}
                    </Text>

                    <Image source={require('../../assets/icon/active-place.png')} style={{width:30, height:30, marginLeft:15}}/>

                    <Text style={{fontSize:15}}>{'N/A'}</Text>
                </View>
            </View>
        </Button>
        
      )

    render()
    {
        return(
            <Container style={{flex:1}} >
                <Header hasTabs style={{ backgroundColor:"#026296" }} androidStatusBarColor="#026296" >
                    <Body>
                        <Title>{'Task List'}</Title>
                    </Body>
                </Header>

                <View style={{ flex:1, flexDirection:'column' }}>
                    <View style={{ flex:1, flexDirection:'column', backgroundColor:'#F5F5F5' }}>
                        <View style={{ flex:1, flexDirection:'column', marginRight:0, marginTop:20 }}>
                        
                        <FlatList 
                                data={ this.state.dataSource }
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={ this.renderItem }
                                onRefresh={ ()=> this.fetchData()}
                                refreshing={this.state.isfetching}
                            />
                        </View>
                    </View>
                </View>
            </Container>

        );
    }
}
import { StyleSheet } from "react-native"

export default StyleSheet.create
({
    labelStatus :
    {
        flex:0.4, 
        flexDirection:'column',
        justifyContent:'center', 
        alignContent:'center', 
        alignItems:'center',
        alignSelf:'center', 
        borderRadius : 50, 
        paddingTop:7, 
        paddingBottom:7,
        width:30,
    },
    container: 
    {
        flex: 1,
        padding: 10,
        backgroundColor: '#FFFFFF'
    },
    
      content: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
    
      datePickerBox:{
        marginTop: 9,
        padding: 0,
        height: 38,
        justifyContent:'center'
      },
    
      datePickerText: {
        fontSize: 14,
        marginLeft: 5,
        borderWidth: 0,
        color: '#000',
  
    
      },
});
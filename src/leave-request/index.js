import React, { Component } from 'react';
import { View, YellowBox, Text, Image, StatusBar, StyleSheet, TextInput, Picker} from 'react-native';
import { Header, Footer, FooterTab, Container, Content, Right, Left, Body, Title, Button, Icon, Tab, Tabs, TabHeading, Card, CardItem, List, ListItem, Form, Item, Label, Input } from 'native-base';
import { Divider, Avatar } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';
import { DatePickerDialog } from 'react-native-datepicker-dialog'
import moment from 'moment';

import styles from './style.js';

export default class LeaveRequest extends Component
{
    constructor(props)
    {
        super(props);

        
        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: isMounted is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);

        console.disableYellowBox = true;

    }


    state = 
    {
        DateTextStart: '',
        DateTextEnd  :'',
        PickerValueHolder : '',
        DateHolder: null,
    };


    DatePickerMainFunctionCallStart = () => 
    {
        let DateHolder = this.state.DateHolder;
    
        if(!DateHolder || DateHolder == null)
        {
            DateHolder = new Date();
            this.setState({
                DateHolder: DateHolder
            });
        }
    
        //To open the dialog
        this.refs.DatePickerDialogStart.open({
            date: DateHolder,
        });
    }

    DatePickerMainFunctionCallEnd = () => 
    {
        let DateHolder = this.state.DateHolder;
    
        if(!DateHolder || DateHolder == null)
        {
            DateHolder = new Date();
            this.setState({
                DateHolder: DateHolder
            });
        }
    
        //To open the dialog
        this.refs.DatePickerDialogEnd.open({
            date: DateHolder,
        });
    }

    onDatePickedFunctionStart = (date) => 
    {
        this.setState({
          dobDate: date,
          DateTextStart: moment(date).format('DD-MMM-YYYY')
        });
      }

      onDatePickedFunctionEnd = (date) => 
      {
          this.setState({
            dobDate: date,
            DateTextEnd: moment(date).format('DD-MMM-YYYY')
          });
        }

    static navigationOptions = 
    {
        tabBarLabel: 'Leave Request',
        // Note: By default the icon is only shown on iOS. Search the showIcon option below.
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require('../../assets/icon/active-home.png')}
            style={{width:40, height:40}}
          />
        ),
        tabBarIcon: ({ focused }) => (
            focused 
            ? <Image source={require('../../assets/icon/active-leaveRequest.png')} style={{width:40, height:40}} />
            : <Image source={require('../../assets/icon/leaveRequest.png')} style={{width:40, height:40}} />
          ),
      };    

      render()
      {
            var task_data = 
            [
                { 
                    requestID   : "REQ-HR-LR-001",
                    date        : "18/4/18 - 20/4/18",
                    status      : "A",
                    note        : "Holiday trip to Bali"
                },
                {
                    requestID   : "REQ-HR-LR-002",
                    date        : "10/4/18 - 16/4/18",
                    status      : "R",
                    note        : "Maternity"
                },
                {
                    requestID   : "REQ-HR-LR-003",
                    date        : "30/8/18 - 10/9/18",
                    status      : "P",
                    note        : "Annual Leave"
                },
                {
                    requestID   : "REQ-HR-LR-004",
                    date        : "15/6/18 - 18/5/18",
                    status      : "A",
                    note        : "Holiday trip to Malang"
                },
                {
                    requestID   : "REQ-HR-LR-005",
                    date        : "20/11/18 - 29/11/18",
                    status      : "A",
                    note        : "Annual Leave"
                }
            ];


            let data = [{
                value: 'Annual Leave',
              }, {
                value: 'Maternity',
              }, {
                value: 'Permit',
              }];

          return(
            <Container style={{flex:1}}>
                <Header androidStatusBarColor="#026296" style={{backgroundColor:"#026296", elevation:0}}> 
                    <Body>
                        <Title>Leave Request</Title>
                    </Body>
                </Header>
                <Tabs initialPage={0} tabBarUnderlineStyle={{backgroundColor: '#1192D8'}} style={{backgroundColor:"#026296"}}  >
                    <Tab    heading="History" 
                            tabStyle={{backgroundColor: '#026296'}} 
                            textStyle={{color: '#fff'}} 
                            activeTabStyle={{backgroundColor: '#026296'}} 
                            activeTextStyle={{color: '#fff', fontWeight: 'normal'}}>
                            <View style={{flex:1, flexDirection:'column', backgroundColor:'#F5F5F5'}}>
                                <List scrollEnabled={true} dataArray={task_data} showsVerticalScrollIndicator={false} renderRow={(item) =>
                                    <ListItem style={{marginLeft:10, marginRight:10, marginTop:6, marginBottom:6, borderRadius:10, backgroundColor:'#fff'}} noBorder>
                                        <View style={{flex:1, flexDirection:'column'}}>
                                            <Text style={{fontSize:20, paddingLeft:20, paddingTop:0, color:'#026296'}}>{item.requestID}</Text>
                                            <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingLeft:20}}>
                                                <Image source={require('../../assets/icon/active-shiftSchedule.png')} style={{width:20, height:20}}/>
                                                <Text style={{fontSize:14, color:'#9E9E9E'}}>{item.date}</Text>
                                            </View>
                                            <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingLeft:20}}>
                                            <Image source={require('../../assets/icon/active-notes.png')} style={{width:20, height:20}}/>
                                                <Text style={{fontSize:14, color:'#9E9E9E'}}>{item.note}</Text>
                                            </View>
                                        </View>
                                        <View style={[styles.labelStatus, { backgroundColor: item.status=='A' ? '#09af00' : item.status=='P' ? '#FFCA28' : item.status=='R' ? '#E53935': '' }] }>
                                            <Text style={{fontSize:13, color:'#fff'}}>{item.status=='A' ? 'Approved' : (item.status=='P') ? 'Pending' : (item.status=='R') ? 'Rejected' : null }</Text>
                                        </View>
                                    </ListItem>
                                    }>
                                </List>
                            </View>           
                    </Tab>
                    <Tab    heading="Request"
                            tabStyle={{backgroundColor: '#026296'}} 
                            textStyle={{color: '#fff'}} 
                            activeTabStyle={{backgroundColor: '#026296'}} 
                            activeTextStyle={{color: '#fff', fontWeight: 'normal'}}>

                            <View style={{flex:1, flexDirection:'column', backgroundColor:'#F5F5F5'}}>
                                <View style={{flex:1, flexDirection:'column', backgroundColor:'#fff', borderRadius:10, marginLeft:20, marginTop:20, marginBottom:20, marginRight:20}}>
                                    <View style={{flex:1, flexDirection:'column', marginTop:10, marginLeft:10, marginRight:10}}> 
                                         {/* <Dropdown
                                                label='Request Type'
                                                data={data}
                                            /> */}
                                            <Text style={{}}>Request Type</Text>
                                            <Picker
                                                color = "#212121"
                                                selectedValue={this.state.PickerValueHolder}
                                                onValueChange={(itemValue, itemIndex) => this.setState({PickerValueHolder: itemValue})} 
                                                style={{borderBottomColor:'#212121', borderBottomWidth:1}}>
                                                {/* <Item label="blue" color="blue" value="blue" /> */}
                                                <Picker.Item label="Choose Request" value="" />
                                                <Picker.Item label="Annual Leave" value="1" />
                                                <Picker.Item label="Maturnity" value="2" />
                                                <Picker.Item label="Permission" value="3" />
                                            </Picker>
                                    </View>
                                    
                                    <View style={{flex:1, flexDirection:'row'}}>
                                        <View style={{flex:1, flexDirection:'column', marginRight:10, marginLeft:10}}>
                                            <Text>From</Text>
                                            <View style={{flex:1, flexDirection:'row'}}>
                                                <Button  iconRight style={{width:'100%',  marginRight:10, backgroundColor:'transparent', borderBottomColor:'#BDBDBD', borderBottomWidth:1, elevation:0}} onPress={this.DatePickerMainFunctionCallStart.bind(this)} >
                                                    <View style={styles.datePickerBox}>
                                                        <Text style={styles.datePickerText}>{this.state.DateTextStart}</Text>
                                                    </View>
                                                    <Icon name="md-calendar" style={{fontSize:20, color:'#BDBDBD'}}/>
                                                </Button> 
                                            </View>
                                        </View>

                                        <View style={{flex:1, flexDirection:'column', marginRight:10, marginLeft:10 }}>
                                            <Text style={{}}>To</Text>
                                            <View style={{flex:1, flexDirection:'row'}}>
                                                <Button  iconRight style={{width:'100%', backgroundColor:'transparent', borderBottomColor:'#BDBDBD', borderBottomWidth:1, elevation:0}} onPress={this.DatePickerMainFunctionCallEnd.bind(this)} >
                                                    <View style={styles.datePickerBox}>
                                                        <Text style={styles.datePickerText}>{this.state.DateTextEnd}</Text>
                                                    </View>
                                                    <Icon name="md-calendar" style={{fontSize:20, color:'#BDBDBD'}}/>
                                                </Button>
                                                
                                            </View> 
                                        </View>

                                        <DatePickerDialog ref="DatePickerDialogStart" onDatePicked={this.onDatePickedFunctionStart.bind(this)} />
                                        <DatePickerDialog ref="DatePickerDialogEnd" onDatePicked={this.onDatePickedFunctionEnd.bind(this)} />
                                    </View>

                                    <View style={{flex:1, flexDirection:'column', marginLeft:10, marginRight:10}}>
                                        <Item stackedLabel>
                                            <Label style={{color:'BDBDBD'}}>Reason</Label>
                                            <Input />
                                        </Item>
                                    </View>


                                    <View style={{flex:1, flexDirection:'row', justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center'}}>
                                       <Button block iconLeft  rounded style={{backgroundColor:'#026296', elevation:0, marginLeft:30, marginRight:30}}>
                                            <Icon name="md-add"/>
                                            <Text style={{color:'#fff', fontSize:15, paddingLeft:20}}> Add Request</Text>
                                        </Button>
                                    </View>
                                    
                               </View>
                            </View>
                    </Tab>
                </Tabs>
            </Container>
          
          );
      }
}

import React, {Component} from 'react';
import { View, Image } from 'react-native';
import { TabNavigator, TabBarBottom } from 'react-navigation';


import Home from '../home/index.js';
import TaskList from '../task-list/index.js';
import ShiftSchedule from '../shift-scheduled/index.js';
import LeaveRequest from '../leave-request/index.js';
import Settings from '../setting/index.js';


export default TabNavigator
(
    {
        Home            :   { screen  : Home,
                                navigationOptions: 
                                {
                                    tabBarLabel: 'Home ',
                                    tabBarIcon: ({ tintColor }) => 
                                    (
                                      <Image
                                        source={require('../../assets/icon/active-home.png')}
                                        style={{width:40, height:40}}
                                      />
                                    ),
                            
                                    tabBarIcon: ({ focused }) => 
                                    (
                                        focused 
                                        ? <Image source={require('../../assets/icon/active-home.png')} style={{width:40, height:40}} />
                                        : <Image source={require('../../assets/icon/home.png')} style={{width:40, height:40}} />
                                    ),
                                },
                            },
        TaskList        : { screen  : TaskList,
                                navigationOptions: 
                                {
                                    tabBarLabel: 'Task List',
                                },
                             },
        ShiftSchedule   : { screen  : ShiftSchedule},
        LeaveRequest    : { screen  : LeaveRequest},
        // Settings        : { screen  : Settings }
    },
    {
        initialRouteName: 'Home',
        tabBarPosition: 'bottom',
        animationEnabled: true,
        tabBarOptions: 
        {
            renderIndicator: () => null,
            showIcon: true,
            showLabel: true,
            labelStyle: {
                fontSize: 10,
              },

            style: 
            {
                backgroundColor: '#fff',
            },
            activeTintColor : 'blue',
            inactiveTintColor  : 'grey'
        },
    }
);
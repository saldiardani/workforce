import React, { Component } from 'react';
import { View, YellowBox, Text, Image, StatusBar, TouchableOpacity, StyleSheet, TextInput, Picker, Slider, Switch, PanResponder, Animated} from 'react-native';
import { Header, Footer, FooterTab, Container, Content, Right, Left, Body, Title, Button, Icon, Tab, Tabs, TabHeading, Card, CardItem, List, ListItem, Form, Item, Label, Input } from 'native-base';
import {Divider} from 'react-native-elements';
import Dash from 'react-native-dash';
import Modal from 'react-native-modalbox';

import Network from '../../config/network';
import Utility from '../../config/utility';

//import Sound from 'react-native-sound';
var Sound = require('react-native-sound');
Sound.setCategory('Playback');

let pop_up = new Sound('intuition.mp3', Sound.MAIN_BUNDLE, (error) => 
{
    if (error) 
    {
      console.log('failed to load the sound', error);
      return;
    }
    else
    {   
        
        console.log('successfully load for begin task sound');
    }
    // loaded successfully
    console.log('duration in seconds: ' + pop_up.getDuration() + 'number of channels: ' + pop_up.getNumberOfChannels());
});

pop_up.setVolume(100);
  //pop_up.setPan(1);
pop_up.setNumberOfLoops(-1);

let id_list     = '';
let EndPoint    = null;

export default class TaskListDetail extends Component
{
    constructor(props)
    {
        super(props);

        YellowBox.ignoreWarnings
        ([
            'Warning: componentWillMount is deprecated',
            'Warning: isMounted is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);
      
        console.disableYellowBox = true;    
        this.id_list = this.props.navigation.state.params.idlist;   
    }

    componentDidMount()
    {
        this.getTaskListDetail(this.id_list);
    }

    state = 
    { 
        pos         : 0,
        isOpen      : false,
        isDisabled  : false,
        swipeToClose: true,
        sliderValue : 0.3,
        
        dataSource  : [],
        start_time  : null,
        end_time    : null,

        taskButton  : false,
        hide_task_button : true
    }

    getTaskListDetail = (id) =>
    {
        //EndPoint = 'http://172.25.230.165/dispatch/task-list/find/'+id;
        EndPoint = 'http://ads-api.asyst.co.id/dispatch/task-list/find/'+id;
    
        Network.reqdatatasklistdetail(EndPoint).then( (response) => 
        {   
            console.log( response );

            this.setState
            ({
                isLoading: false,
                dataSource: response,
            });

            this.setState
            ({
                start_time  : Utility.getHourMinute(this.state.dataSource.startTime),
                end_time    : Utility.getHourMinute(this.state.dataSource.endTime),
            })

            let status_task_temp = this.state.dataSource.status;

            if(status_task_temp=='Dispatch')
            {
                this.setState({hide_task_button:false});
                this.setState({taskButton:false});
            }
            
            if(status_task_temp=='Start')
            {
                this.setState({hide_task_button:false, taskButton:true});
            }

            if(status_task_temp=='Finish')
            {
                this.setState({hide_task_button:true});
            }

            console.log(this.state.taskButton);
        });
    }

    beginTask = () => 
    {
        //EndPoint = 'http://172.25.230.165/dispatch/task-list/start/'+this.id_list;

        EndPoint = 'http://ads-api.asyst.co.id/dispatch/task-list/start/'+this.id_list;
        
        Network.reqstarttask(EndPoint).then( (response)=> 
        {
            console.log(response);
        } );
        this.setState({taskButton:true});
        this.refs.modal1.close();
    }

    endTask = () =>
    {
        //EndPoint = 'http://172.25.230.165/dispatch/task-list/end/'+this.id_list;

        EndPoint = 'http://ads-api.asyst.co.id/dispatch/task-list/end/' + this.id_list;
        
        Network.reqstarttask(EndPoint)
        .then( (response)=> 
        {
            console.log(response);

        } );
        
        this.setState({hide_task_button:true});
        this.refs.modal2.close();
    }

    openModalBeginTask = () =>
    {
        pop_up.play((success) => 
        {
            if (success) 
            {
                pop_up.setCurrentTime(0);
                console.log('successfully finished playing');
            } 
            else 
            {
                console.log('playback failed due to audio decoding errors');
                pop_up.reset();
            }
          });

        this.refs.modal1.open();
    }
    openModalEndTask = () =>
    {
        pop_up.play((success) => 
        {
            if (success) 
            {
                pop_up.setCurrentTime(0);
                console.log('successfully finished playing');
            } 
            else 
            {
                console.log('playback failed due to audio decoding errors');
                pop_up.reset();
            }
          });

        this.refs.modal2.open();
    }


    back = () =>
    {
        this.props.navigation.navigate('Dashboard');
    }

   
  
    
    onClose = () => 
    {
        console.log('Modal just closed');
    }
    
    onOpen = () => 
    {
        console.log('Modal just openned');
        
    }
    
    onClosingState = (state) =>
    {
        console.log('the open/close of the swipeToClose just changed');
    }

    render()
    {    
        return(
            <Container style={{flex:1, backgroundColor:'#F5F5F5'}}>
                <Header androidStatusBarColor="#026296" style={{backgroundColor:"#026296", elevation:0}}> 
                    <Left>
                        <Button transparent onPress={this.back}>
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>

                    <Body>
                        <Title>Task List Detail</Title>
                    </Body>
                </Header>
                <Content>
                    <View style={{flex:1, flexDirection:'column'}}>

                        <View style={{flex:1, flexDirection:'column', backgroundColor:'#fff', borderRadius:10, marginTop:20, marginLeft:20, marginRight:20}}>
                            <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-airplane.png')} style={{width:40,height:40}}/>
                                <Text style={{fontSize:20, color:'#026296'}}>Task Detail</Text>
                            </View>

                             <View style={{flex:1, flexDirection:'column', paddingLeft:40, paddingRight:32}}>
                                <Divider style={{backgroundColor:'#E0E0E0'}} />
                            </View>

                            <View style={{flex:1, flexDirection:'row', paddingLeft:20, paddingTop:10}}>
                        
                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-time.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>start</Text>
                                        <Text style={{fontSize:17}}>{ this.state.start_time }</Text>
                                    </View>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-time.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>end</Text>
                                        <Text style={{fontSize:17}}>{ this.state.end_time }</Text>
                                    </View>
                                </View>
                            </View>

                            <View style={{flex:1, flexDirection:'row', paddingLeft:20, paddingTop:10, paddingBottom:10}}>
                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-place.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>from</Text>
                                        <Text style={{fontSize:17}}>N/A</Text>
                                    </View>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-place.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>to</Text>
                                        <Text style={{fontSize:17}}>N/A</Text>
                                    </View>
                                </View>
                            </View >


                            <View style={{flex:1, flexDirection:'column', paddingLeft:20, paddingTop:10, paddingBottom:10}}>
                                <View style={{flex:1, flexDirection:'column', marginLeft:20, marginRight:20}}>
                                    <Text>Description</Text>
                                    
                                    <View style={{flex:1, flexDirection:'column', marginTop:7, marginBottom:8}}>
                                        <Divider style={{backgroundColor:'#E0E0E0'}} />
                                    </View>

                                    <Text style={{fontSize:14}}>
                                        {this.state.dataSource.ruleDesc}
                                    </Text>
                                </View>
                            </View>
                        </View>



                        <View style={{flex:1, flexDirection:'column', backgroundColor:'#fff', borderRadius:10, marginTop:20, marginLeft:20, marginRight:20}}>
                            <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingBottom:6, paddingLeft:7, paddingTop:10}}>
                                {/* <Image source={require('../../assets/icon/active-airplane.png')} style={{width:40,height:40}}/> */}
                                <Icon name="ios-disc" style={{color:'#026296', fontSize:15, paddingLeft:7, paddingRight:13}} />
                                <Text style={{fontSize:20, color:'#026296'}}>Arrival Detail</Text>
                            </View>

                             <View style={{flex:1, flexDirection:'column', paddingLeft:40, paddingRight:32}}>
                                <Divider style={{backgroundColor:'#E0E0E0'}} />
                            </View>

                            <View style={{flex:1, flexDirection:'row', paddingLeft:20, paddingTop:10}}>
                                <View style={{flex:0.1, flexDirection:'row'}}>
                                    <Dash style={{width:1, height:50, flexDirection:'column'}} dashColor="#E0E0E0"/>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>flight</Text>
                                        <Text style={{fontSize:17}}>{this.state.dataSource.airline+' '+this.state.dataSource.tripNumber}</Text>
                                    </View>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-time.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>schedule time</Text>
                                        <Text style={{fontSize:17}}>N/A</Text>
                                    </View>
                                </View>
                            </View>

                            
                            <View style={{flex:1, flexDirection:'row', paddingLeft:20, paddingTop:0}}>
                                <View style={{flex:0.1, flexDirection:'row'}}>
                                    <Dash style={{width:1, height:50, flexDirection:'column'}} dashColor="#E0E0E0"/>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>   
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>Route</Text>
                                        <Text style={{fontSize:17}}>DPS</Text>
                                    </View>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-time.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>estimate time</Text>
                                        <Text style={{fontSize:17}}>N/A</Text>
                                    </View>
                                </View>
                            </View>


                            <View style={{flex:1, flexDirection:'row', paddingLeft:20, paddingTop:0, paddingBottom:10}}>
                                <View style={{flex:0.1, flexDirection:'row'}}>
                                    <Dash style={{width:1, height:50, flexDirection:'column'}} dashColor="#E0E0E0"/>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>Aircraft</Text>
                                        <Text style={{fontSize:17}}>{this.state.dataSource.acType}</Text>
                                    </View>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-time.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>actual time</Text>
                                        <Text style={{fontSize:17}}>N/A</Text>
                                    </View>
                                </View>
                            </View>

                             <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingBottom:4, paddingLeft:7, paddingTop:0}}>
                                {/* <Image source={require('../../assets/icon/active-airplane.png')} style={{width:40,height:40}}/> */}
                                <Icon name="ios-disc" style={{color:'#026296', fontSize:15, paddingLeft:7, paddingRight:13}} />
                                <Text style={{fontSize:20, color:'#026296'}}>Depature Detail</Text>
                            </View>

                             <View style={{flex:1, flexDirection:'column', paddingLeft:40, paddingRight:32}}>
                                <Divider style={{backgroundColor:'#E0E0E0'}} />
                            </View>

                            <View style={{flex:1, flexDirection:'row', paddingLeft:20, paddingTop:10}}>
                                <View style={{flex:0.1, flexDirection:'row'}}>
                                    <Dash style={{width:1, height:50, flexDirection:'column'}} dashColor="#E0E0E0"/>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>flight</Text>
                                        <Text style={{fontSize:17}}>{this.state.dataSource.airline+' '+this.state.dataSource.tripNumber}</Text>
                                    </View>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-time.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>schedule time</Text>
                                        <Text style={{fontSize:17}}>N/A</Text>
                                    </View>
                                </View>
                            </View>

                            
                            <View style={{flex:1, flexDirection:'row', paddingLeft:20, paddingTop:0}}>
                                <View style={{flex:0.1, flexDirection:'row'}}>
                                    <Dash style={{width:1, height:50, flexDirection:'column'}} dashColor="#E0E0E0"/>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>   
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>Route</Text>
                                        <Text style={{fontSize:17}}>DPS</Text>
                                    </View>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-time.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>estimate time</Text>
                                        <Text style={{fontSize:17}}>N/A</Text>
                                    </View>
                                </View>
                            </View>


                            <View style={{flex:1, flexDirection:'row', paddingLeft:20, paddingTop:0, paddingBottom:5}}>
                                <View style={{flex:0.1, flexDirection:'row'}}>
                                    <Dash style={{width:1, height:50, flexDirection:'column'}} dashColor="#E0E0E0"/>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>Aircraft</Text>
                                        <Text style={{fontSize:17}}>{this.state.dataSource.acType}</Text>
                                    </View>
                                </View>

                                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                <Image source={require('../../assets/icon/active-time.png')} style={{width:40, height:40}}/>
                                    <View style={{flex:1, flexDirection:'column'}}>
                                        <Text>actual time</Text>
                                        <Text style={{fontSize:17}}>N/A</Text>
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={{flex:1, flexDirection:'row',  justifyContent:'center', marginLeft:20, marginRight:20, marginTop:20, marginBottom:20, height:60}}>
                             {
                                this.state.hide_task_button ? 
                                    <View />

                                :
                                    
                                    this.state.taskButton ?

                                    <Button rounded block style={{flex:1, backgroundColor:'#B71C1C', elevation:0}} onPress={ this.openModalEndTask }>
                                        <Text style={{color:'#fff', elevation:0, fontSize:18}}>End Task</Text>
                                    </Button>

                                :
                                    <Button rounded block style={{flex:1, backgroundColor:'#00A84E', elevation:0}} onPress={ this.openModalBeginTask }>
                                        <Text style={{color:'#fff', elevation:0, fontSize:18}}>Begin Task</Text>
                                    </Button> 
                             }
                        </View>

                    </View>
                     
                </Content>

                <Modal
                    style={{width:'70%', flex:0.3, flexDirection:'column', borderRadius:10}}
                    ref={"modal1"}
                    swipeToClose={this.state.swipeToClose}
                    onClosed={this.onClose}
                    onOpened={this.onOpen}
                    onClosingState={this.onClosingState}>
                        <View style={{flex:0.2, flexDirection:'column', justifyContent:'center', alignContent:'center', marginTop:20}}>
                            <Text style={{color:'#026296', textAlign:'center', fontSize:20}}>Confirmation</Text>
                        </View>

                        <View style={{flex:0.2, flexDirection:'column', justifyContent:'center', alignContent:'center'}}>
                            <Divider />
                        </View>

                        <View style={{flex:0.4, flexDirection:'column', justifyContent:'center', alignContent:'center'}}>
                            <Text style={{color:'#212121', textAlign:'center', fontSize:17}}>Begining the task ?</Text>
                        </View>
                        

                        <View style={{flex:0.3, flexDirection:'row', justifyContent:'center', alignContent:'center', marginBottom:30, marginTop:20}}>
                            <Button rounded style={{backgroundColor:'#00A84E', marginRight:10, elevation:0}} onPress={ this.beginTask }>
                                <Text style={{paddingLeft:20, paddingRight:20, fontSize:16, color:'#fff'}}>Begin</Text>
                            </Button>

                            <Button rounded style={{backgroundColor:'#EEEEEE', marginLeft:10, elevation:0}} onPress={() => this.refs.modal1.close() }>
                                <Text style={{paddingLeft:20, paddingRight:20, fontSize:16}}>Not Now</Text>
                            </Button>
                        </View>
                </Modal>


                {/* <Modal
                    style={{width:'80%', flex:0.5, flexDirection:'column', borderRadius:10}}
                    ref={"modal2"}
                    swipeToClose={this.state.swipeToClose}
                    onClosed={this.onClose}
                    onOpened={this.onOpen}
                    onClosingState={this.onClosingState}>
                        <View style={{flex:0.1, flexDirection:'column', justifyContent:'center', alignContent:'center', marginTop:20, marginBottom:20}}>
                            <Text style={{color:'#026296', textAlign:'center', fontSize:20}}>Confirmation</Text>
                        </View>

                        <View style={{flex:0.1, flexDirection:'column', justifyContent:'center', alignContent:'center'}}>
                            <Divider />
                        </View>

                        <View style={{flex:0.4, flexDirection:'column', justifyContent:'center', alignContent:'center', marginTop:10}}>
                            <Text style={{color:'#212121', textAlign:'center', fontSize:17}}> if you have any trouble, need time extended and so on. Please do fill feedback. ?</Text>
                        </View>
                        
                        <View style={{flex:0.5, flexDirection:'column', justifyContent:'center', alignContent:'center', marginTop:5, marginRight:20}}>
                            <Form>
                                <Item stackedLabel>
                                    <Label>Feedback</Label>
                                    <Input />
                                </Item>
                            </Form>
                        </View>

                        
                        <View style={{flex:0.3, flexDirection:'row', justifyContent:'center', alignContent:'center', marginBottom:20, marginTop:20, marginLeft:20, marginRight:20}}>
                            <Button rounded style={{backgroundColor:'#B71C1C', marginRight:10, elevation:0}} onPress={ this.endTask }>
                                <Text style={{paddingLeft:20, paddingRight:20, fontSize:16, color:'#fff'}}>Send</Text>
                            </Button> 
                            
                            <Button rounded style={{backgroundColor:'#EEEEEE', marginRight:10, elevation:0}} onPress={ this.endTask }>
                                <Text style={{paddingLeft:20, paddingRight:20, fontSize:16}}>Just End</Text>
                            </Button>

                            <Button rounded style={{backgroundColor:'#EEEEEE', marginLeft:10, elevation:0}} onPress={() => this.refs.modal2.close() }>
                                <Text style={{paddingLeft:20, paddingRight:20, fontSize:16}}>Not Now</Text>
                            </Button>
                        </View>
                </Modal> */}

                <Modal
                    style={{width:'70%', flex:0.3, flexDirection:'column', borderRadius:10}}
                    ref={"modal2"}
                    swipeToClose={this.state.swipeToClose}
                    onClosed={this.onClose}
                    onOpened={this.onOpen}
                    onClosingState={this.onClosingState}>

                        <View style={{flex:0.2, flexDirection:'column', justifyContent:'center', alignContent:'center', marginTop:20}}>
                            <Text style={{color:'#026296', textAlign:'center', fontSize:20}}>Confirmation</Text>
                        </View>

                        <View style={{flex:0.2, flexDirection:'column', justifyContent:'center', alignContent:'center'}}>
                            <Divider />
                        </View>

                        <View style={{flex:0.4, flexDirection:'column', justifyContent:'center', alignContent:'center'}}>
                            <Text style={{color:'#212121', textAlign:'center', fontSize:17}}>End Task now ?</Text>
                        </View>
                        

                        <View style={{flex:0.3, flexDirection:'row', justifyContent:'center', alignContent:'center', marginBottom:30, marginTop:20}}>
                            <Button rounded style={{backgroundColor:'#B71C1C', marginRight:10, elevation:0}} onPress={ this.endTask }>
                                <Text style={{paddingLeft:20, paddingRight:20, fontSize:16, color:'#fff'}}>End Task</Text>
                            </Button>

                            <Button rounded style={{backgroundColor:'#EEEEEE', marginLeft:10, elevation:0}} onPress={() => this.refs.modal2.close() }>
                                <Text style={{paddingLeft:20, paddingRight:20, fontSize:16}}>Not Now</Text>
                            </Button>
                        </View>
                </Modal>
            </Container>
        );
    }
}

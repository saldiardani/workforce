import React, { Component } from 'react';
import { View, YellowBox, Text, Image, StatusBar, TouchableOpacity, StyleSheet, TextInput, Picker} from 'react-native';
import { Header, Footer, FooterTab, Container, Content, Right, Left, Body, Title, Button, Icon, Tab, Tabs, TabHeading, Card, CardItem, List, ListItem, Form, Item, Label, Input } from 'native-base';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

import Util from '../../config/utility';

export default class ShiftScheduled extends Component
{

    constructor(props)
    {
        super(props);

        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: isMounted is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);

        console.disableYellowBox = true;

        global.currentDate = Util.getFormatDay(Util.getCurrentDay(),Util.getCurrentMonth(), Util.getCurrentYear(), 5);
        global.minDate     = Util.getMinDateOfMonth();
        global.maxDate     = Util.getLastDayOfMonth().toString();
    }

    
    state = 
    {
        selected_date: '',
        data : []
    }


    static navigationOptions = 
    {
        tabBarLabel: 'Shift Schedule',
        // Note: By default the icon is only shown on iOS. Search the showIcon option below.
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require('../../assets/icon/active-shiftSchedule.png')}
            style={{width:40, height:40}}
          />
        ),
        tabBarIcon: ({ focused }) => (
            focused 
            ? <Image source={require('../../assets/icon/active-shiftSchedule.png')} style={{width:40, height:40}} />
            : <Image source={require('../../assets/icon/shiftSchedule.png')} style={{width:40, height:40}} />
          ),
          
      };

    render()
    {
       
        var dataArray =
        {
            // '2018-05-03': {selected: true, marked: true, selectedColor: 'blue'},
            '2018-05-17': {marked: true},
            //'2018-05-18': {marked: true, dotColor: 'red', activeOpacity: 0},
            '2018-05-19': {disabled: true, disableTouchEvent: true}
        };


        var task_data = 
        [
            { 
                shift   : "Shift 0800 - 0900",
                note    : "Lorem ipsum dolor sit amet",
            },
            {
                shift   : "Shift 1000 - 1300",
                note    : "Lorem ipsum dolor sit amet",
            },
            {
                shift   : "Shift 1500 - 1700",
                note    : "Lorem ipsum dolor sit amet",
            },
            {
                shift   : "Shift 2000 - 2100",
                note    : "Lorem ipsum dolor sit amet",
            },
            {
                shift   : "Shift 2200 - 2300",
                note    : "Lorem ipsum dolor sit amet",
            }
        ];

        return(
            <Container style={{flex:1}}>
                <Header androidStatusBarColor="#026296" style={{backgroundColor:"#026296", elevation:0}}> 
                    <Body>
                        <Title>Shift Schedule</Title>
                    </Body>
                </Header>
                {/* <Content> */}
                <View style={{ flex:1, flexDirection:'column' }}>
                    <View style={{ flex:1, flexDirection:'column'
                }}>
                    <Calendar
                        // Initially visible month. Default = Date()
                        current={ currentDate }
                        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                        minDate={[ minDate ]} 
                        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                        maxDate={ maxDate }

                        // Handler which gets executed on day press. Default = undefined
                        // onDayPress={(day) => 
                        // {
                        //     console.log('selected day : ', day)}
                        // }
                        // Handler which gets executed on day long press. Default = undefined
                     onDayLongPress={(day) => 
                        {
                            //console.log('selected day', day)
                            //console.log('pressed is : ' + day.dateString);
                            var selected_day = day.dateString;

                            
                        }}
                        // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                        monthFormat={'MMMM yyyy'}
                        // Handler which gets executed when visible month changes in calendar. Default = undefined
                        onMonthChange={(month) => {console.log('month changed', month)}}
                        // Hide month navigation arrows. Default = false
                        hideArrows={true}
                        // Replace default arrows with custom ones (direction can be 'left' or 'right')
                        renderArrow={(direction) => (<Arrow />)}
                        // Do not show days of other months in month page. Default = false
                        hideExtraDays={true}
                        // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                        // day from another month that is visible in calendar page. Default = false
                        disableMonthChange={false}
                        // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                        firstDay={1}
                        // Hide day names. Default = false
                        hideDayNames={false}
                        // Show week numbers to the left. Default = false
                        showWeekNumbers={true}
                        // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                        onPressArrowLeft={substractMonth => substractMonth()}
                        // Handler which gets executed when press arrow icon left. It receive a callback can go next month
                        onPressArrowRight={addMonth => addMonth()}

                        markedDates={dataArray}

                        // Enable horizontal scrolling, default = false


                        theme={{
                        backgroundColor: '#026296',
                        calendarBackground: '#026296',
                        textSectionTitleColor: '#4FC3F7',
                        selectedDayBackgroundColor: '#ffffff',
                        selectedDayTextColor: '#ffffff',
                        todayTextColor: '#FFEE58',
                        dayTextColor: '#B3E5FC',
                        textDisabledColor: '#0277BD',
                        dotColor: '#DEFC00',
                        selectedDotColor: '#212121',
                        arrowColor: 'orange',
                        monthTextColor: '#F5F5F5',
                        // textDayFontFamily: 'monospace',
                        // textMonthFontFamily: 'monospace',
                        // textDayHeaderFontFamily: 'monospace',
                        // textMonthFontWeight: 'bold',
                        textDayFontSize: 16,
                        textMonthFontSize: 16,
                        textDayHeaderFontSize: 16
                        }}
                        />


                        {/* <Agenda
                            // the list of items that have to be displayed in agenda. If you want to render item as empty date
                            // the value of date key kas to be an empty array []. If there exists no value for date key it is
                            // considered that the date in question is not yet loaded
                            items={
                                {'2018-05-01': [{text: 'item 1 - any js object'}],
                                '2018-05-23': [{text: 'item 2 - any js object'}],
                                '2018-05-24': [],
                                '2018-05-25': [{text: 'item 3 - any js object'},{text: 'any js object'}],
                                }}
                            // callback that gets called when items for a certain month should be loaded (month became visible)
                            loadItemsForMonth={(month) => {console.log('trigger items loading')}}
                            // callback that fires when the calendar is opened or closed
                            onCalendarToggled={(calendarOpened) => {console.log(calendarOpened)}}
                            // callback that gets called on day press
                            onDayPress={(day)=>{console.log('day pressed')}}
                            // callback that gets called when day changes while scrolling agenda list
                            onDayChange={(day)=>{console.log('day changed')}}
                            // initially selected day
                            selected={'2018-05-16'}
                            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                            minDate={'2018-05-01'}
                            // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                            maxDate={'2018-05-30'}
                            // Max amount of months allowed to scroll to the past. Default = 50
                            pastScrollRange={50}
                            // Max amount of months allowed to scroll to the future. Default = 50
                            futureScrollRange={50}
                            // specify how each item should be rendered in agenda
                            renderItem={(item, firstItemInDay) => {return (<View />);}}
                            // specify how each date should be rendered. day can be undefined if the item is not first in that day.
                            renderDay={(day, item) => {return (<View />);}}
                            // specify how empty date content with no items should be rendered
                            renderEmptyDate={() => {return (<View />);}}
                            // specify how agenda knob should look like
                            renderKnob={() => {return (<View />);}}
                            // specify what should be rendered instead of ActivityIndicator
                            renderEmptyData = {() => {return (<View />);}}
                            // specify your item comparison function for increased performance
                            rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}
                            // Hide knob button. Default = false
                            hideKnob={true}
                            // By default, agenda dates are marked if they have at least one item, but you can override this if needed
                            markedDates={{
                                '2018-05-16': {selected: true, marked: true},
                                '2018-05-17': {marked: true},
                                '2018-05-18': {disabled: true}
                            }}
                            // agenda theme
                            // theme={{
                            //     agendaDayTextColor: 'yellow',
                            //     agendaDayNumColor: 'green',
                            //     agendaTodayColor: 'red',
                            //     agendaKnobColor: 'blue'
                            // }}
                            // agenda container style
                            /> */}
                    </View>
                </View>

                    <View style={{flex:1, flexDirection:'column', marginRight:0, marginTop:200, zIndex:-1}}>
                        <List scrollEnabled={true} dataArray={task_data} showsVerticalScrollIndicator={false} renderRow={(item) =>
                            <ListItem style={{marginLeft:10, marginRight:10, marginTop:6, marginBottom:6, borderRadius:10, backgroundColor:'#fff', borderLeftWidth:7, borderColor:"#026296"}} noBorder>
                                <View style={{flex:10, flexDirection:'column', backgroundColor:'#fff', height:50 }}>
                                    {/* <Image source={require('../../assets/icon/active-leaveRequest.png')} style={{width:50, height:50}}/> */}
                                    <Text style={{fontSize:20, paddingLeft:20, paddingTop:0, color:'#026296'}}>{item.shift}</Text>

                                    <View style={{flex:0.8, flexDirection:'row', paddingLeft:16, marginTop:8, alignContent:'center', alignItems:'center'}}>
                                        <Image source={require('../../assets/icon/active-notes.png')} style={{width:30, height:30}}/>
                                        <Text style={{fontSize:15}}>{item.note}</Text>

                                        {/* <Image source={require('../../assets/icon/active-place.png')} style={{width:30, height:30, marginLeft:15}}/>
                                        <Text style={{fontSize:15}}>{item.place}</Text> */}
                                    </View>
                                </View>                                        
                            </ListItem>
                            }>
                        </List>
                    </View>   

                {/* </Content> */}
            </Container>
        );
    }s


}
import { StyleSheet } from "react-native"

export default StyleSheet.create
({
   container : 
   {
        flex:1
   },

   header :
   {
        backgroundColor:"#026296"
   },

   todayNotifContainer :
   {
        flex:0.5, 
        flexDirection:'column', 
        marginLeft:30, 
        marginRight:20, 
        marginTop:20
   },

   filterContainer :
   {
        flex:2, 
        flexDirection:'row', 
        alignContent:'center', 
        justifyContent:'center', 
        marginLeft:30, 
        marginRight:20, 
        marginTop:20,
        marginBottom:20
   },

   buttonFilterPosition :
   {
        flex:1, 
        flexDirection:'row', 
        alignContent:'center', 
        justifyContent:'center'
   },

   buttonFilter :
   {
        padding:20, 
        backgroundColor:'transparent',
        elevation: 0,
        borderWidth:1, 
        borderColor:"#BDBDBD"
   },


   filterImage :
   {
        width:50,
        height:50,
   },

   contentPlace :
   {
        flex:1, 
        flexDirection:'column', 
        backgroundColor:'#F5F5F5'
   },

   infoAccountPosition :
   {
        flex:3, 
        flexDirection:'column', 
        alignContent:'center', 
        justifyContent:'center', 
        paddingLeft:20
   },

   textUsername :
   {
        color:'#026296', 
        fontSize:24
   },

   textJobPosition :
   {
        color:'#9E9E9E',
        fontSize:15
   },

   dividerPosition :
   {
        flex:0.1, 
        flexDirection:'column', 
        justifyContent:'flex-start', 
        marginLeft:20, 
        marginTop:20, 
        marginRight:20
   },

   dividerColor :
   {
        backgroundColor: '#E0E0E0'
   },

   listPosition :
   {
        flex:1, 
        flexDirection:'column', 
        marginRight:0
   }

})
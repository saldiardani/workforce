import React, { Component } from 'react';
import { View, YellowBox, Text, Image, StatusBar, StyleSheet, ListView, FlatList, ActivityIndicator, AsyncStorage,BackHandler,
    ToastAndroid, RefreshControl, Dimensions } from 'react-native';
import { Header, Footer, FooterTab, Container, Content, Right, Left, Body, Title, Button, Icon, Tab, Tabs, TabHeading, Card, CardItem, List, ListItem } from 'native-base';
import { Divider, Avatar } from 'react-native-elements';
import { StackNavigator, TabNavigator } from 'react-navigation';
import Storage from 'react-native-storage';

import AppProperty from '../../config/property.js';
import StaticString from '../../config/string.js';
import Network from '../../config/network';
import styles from './style.js';
import Utility from '../../config/utility';

let randomColor = Utility.getRandomColor();
let employeeID = '';
let currentDate = '';
let dateAPI     = '';
let username    = '';
let jobtitle    = '';

const BannerWidth = Dimensions.get('window').width;

let storage = new Storage({
    size: 1000,
    storageBackend: AsyncStorage,
    defaultExpires: 1000 * 3600 * 24,
    enableCache: true,
    sync : {}
});

export default class Home extends Component
{
    constructor(props)
    {
        super(props);

        AppProperty.setHeaderTitle("Home");
        AppProperty.setSubHeaderTitle("Today Notification");

        currentDate_temp = Utility.getCurrentDate();

        currentDate = currentDate_temp[0] + ' ' +Utility.getMonthString(currentDate_temp[1]) + ' '  + currentDate_temp[2];

        let day    = (currentDate_temp[0] < 10) ? '0' + currentDate_temp[0] : currentDate_temp[0];
        let month  = (currentDate_temp[1] < 10) ? '0' + currentDate_temp[1] : currentDate_temp[1];
       
        dateAPI = currentDate_temp[2] + '-' + month + '-' + day
    }

    state=
    { 
        tasklist_filter : false,
        shift_schedule  : false,
        leave_request   : false,
        alldata         : [],
        isloading       : true,
        dataSource      : [],
        employeeID      : null,
        currentDate     : null,
        id_list         : null,
        shift_type      : null,
        isfetching      : false
    }

    componentWillUnmount() 
    {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() 
    {
        //ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    loadStorage = () =>
    {
        storage.load
        ({
            key: 'loginState',
            autoSync: true,
            syncInBackground: true,
            syncParams: 
            {
              extraFetchOptions: 
              {
                // blahblah
              },
              someFlag: true,
            },
        })
        
        .then(ret => 
        {
                console.log('here-2');

                console.log(ret.userid);
                console.log(ret.username);
                console.log(ret.token);
        
                username = ret.username;
                jobtitle = ret.roleDesc;
                let userid = ret.userid;
                let token = ret.token;  
                
                //EndPoint = 'http://172.25.230.165/dispatch/task-list/list/' + userid + '/2018-06-25';
                EndPoint = 'http://ads-api.asyst.co.id/dispatch/task-list/list/' + userid + '/' + dateAPI;

                console.log('EndPoint HOME URL : '+EndPoint);
                Network.reqdatalistdashboard(EndPoint)
                .then( (response) => 
                {
                    console.log('here-3');

                    if(response=='')
                    {
                        console.log('here-4');
                        console.warn('empty')

                        this.setState({
                            shift_type:'N/A'
                        })
                    }
                    else
                    {
                        console.log('here-5');
                        this.setState({
                            isLoading   : false,
                            isfetching  : false,
                            dataSource  : response,
                            shift_type  : this.state.dataSource.shiftTypeCode 
                        });
                    }

                    //console.log(this.state.dataSource.shiftTypeCode);
                    //console.log(this.state.dataSource);
                });
        })
        
        .catch(err => 
        {
                console.log('here-6');
                console.warn(err.message);
                switch (err.name) 
                {
                    case 'NotFoundError':
                        // TODO;
                        break;
                    case 'ExpiredError':
                        // TODO
                        break;
                }
        });
    }

    fetchData = () =>
    {
        this.setState({isfetching:true});
        this.loadStorage();
    }
    
    componentDidMount()
    {
        this.fetchData();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    renderItem = ({ item }) => 
    (
      
      <Button style={{ backgroundColor:'#fff', height:70, marginLeft:20, marginRight:20, marginTop:6, marginBottom:6, borderRadius:10, backgroundColor:'#fff', borderLeftWidth:7, borderColor:"#026296" }} onPress={this.listPressed.bind(this,item.id)} transparent>
          <View style={{ flex:10, flexDirection:'column' }} >
              <Text style={{ fontSize:20, paddingLeft:20, paddingTop:10, color:'#026296' }}> {item.qualificationCode} </Text>

              <View style={{ flex:0.8, flexDirection:'row', paddingLeft:16, marginTop:8, alignContent:'center', alignItems:'center' }}>
                  <Image source={require('../../assets/icon/active-time.png')} style={{ width:30, height:30 }}/>

                  <Text style={{ fontSize:15 }}>
                      { Utility.getHourMinute(item.startTime) + ' - '+ Utility.getHourMinute(item.endTime)}
                  </Text>

                  <Image source={require('../../assets/icon/active-place.png')} style={{width:30, height:30, marginLeft:15}}/>

                  <Text style={{fontSize:15}}>{'N/A'}</Text>
              </View>
          </View>
      </Button>
      
    )

      filterTaskList = () =>
        {
            this.setState
            ({
                tasklist_filter  : !this.state.tasklist_filter,
                shift_schedule   : false,
                leave_request    : false 
            });

            console.log(this.state.tasklist_filter);
        }

        filterShiftSchedule = () =>
        {
            this.setState
            ({
                tasklist_filter  : false,
                shift_schedule   : !this.state.shift_schedule,
                leave_request    : false 
            });

            console.log(this.state.shift_schedule);
        }

        filterLeaveRequest = () =>
        {
            this.setState
            ({
                tasklist_filter  : false,
                shift_schedule   : false,
                leave_request    : !this.state.leave_request
            });

            console.log(this.state.leave_request);
        }

        listPressed = (id) => 
        {

            this.setState
            (
                {
                    id_list : id 
                }, 
                ()=>
                {
                    this.setState({ id_list: id });
                    console.log('id_list : '+this.state.id_list);

                    this.props.navigation.navigate('TaskListDetail', {idlist : this.state.id_list });
                }
            );
        }

    render()
    {   
        return(
            <Container style={styles.containter}>
                <Header hasTabs style={styles.header} androidStatusBarColor="#026296" >
                    <Body>
                        <Title>{AppProperty.getHeaderTitle()}</Title>
                    </Body>
                </Header>
                
                <Content style={{ backgroundColor:'#F5F5F5' }}>
                    <View style={styles.contentPlace}>
                        <View style={{flex:7, flexDirection:'column', backgroundColor:'#fff', borderRadius:10, marginTop:20, marginLeft:20, marginRight:20}}>
                            <View style={{flex:0.5, flexDirection:'row', paddingLeft:20, paddingTop:20}}>
                                <View style={{flex:0.8, flexDirection:'column', alignContent:'center', alignItems:'center', justifyContent:'center'}} >
                                    <Avatar
                                        medium
                                        rounded
                                        title={Utility.getFirstAlphabentEachWord(username)}
                                        activeOpacity={0}
                                        containerStyle={{backgroundColor:[randomColor]}}
                                        />
                                </View>
                                
                                <View style={styles.infoAccountPosition} >
                                    <Text style={styles.textUsername}>{Utility.getUsername(username)}</Text>
                                    <Text style={styles.textJobPosition}>{jobtitle}</Text>
                                </View>
                            </View>

                            <View style={styles.dividerPosition}>
                                <Divider style={styles.dividerColor} />
                            </View>

                            <View style={{flex:5,flexDirection:'row', paddingLeft:20, height:70}}>
                                <View style={{flex:1,flexDirection:'row'}}>
                                    <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>
                                        <Text style={{color:'#9E9E9E'}}>Date</Text>
                                        <Text style={{fontSize:17, color:'#026296', fontWeight:'normal'}} >{currentDate}</Text>
                                    </View>
                                </View>
                                

                                <View style={{flex:1,flexDirection:'row'}}>
                                    <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>
                                            <Text style={{color:'#9E9E9E'}}>Shift</Text>
                                            <Text style={{fontSize:17, color:'#026296', fontWeight:'normal'}}>{this.state.shift_type}</Text>
                                    </View>
                                </View>

                            </View>
                        </View>

                         <View style={styles.todayNotifContainer}>
                            <Text>{ AppProperty.getSubHeaderTitle() }</Text>
                        </View>

                         <View style={styles.filterContainer}>
                            <View style={styles.buttonFilterPosition}>
                                <Button rounded light style={[styles.buttonFilter, { backgroundColor: this.state.tasklist_filter ? '#014C75' : '#ffffff' }]} onPress={this.filterTaskList}>
                                    <Image source={require('../../assets/icon/active-taskList.png')} style={styles.filterImage}/>
                                </Button>
                            </View>

                            <View style={styles.buttonFilterPosition}>
                                <Button rounded light style={[styles.buttonFilter, { backgroundColor: this.state.shift_schedule ? '#014C75' : '#ffffff' }]} onPress={this.filterShiftSchedule}>
                                    <Image source={require('../../assets/icon/active-shiftSchedule.png')} style={styles.filterImage}/>
                                </Button>
                            </View>

                            <View style={styles.buttonFilterPosition}>
                                <Button rounded light style={[styles.buttonFilter, { backgroundColor: this.state.leave_request ? '#014C75' : '#ffffff' }]} onPress={this.filterLeaveRequest}>
                                    <Image source={require('../../assets/icon/active-leaveRequest.png')} style={styles.filterImage}/>
                                </Button>
                            </View>
                        </View>
                        
                        <View style={styles.listPosition}>
                            {/* <FlatList 
                                data={ this.state.dataSource }
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={ this.renderItem }
                                onRefresh={ ()=> this.fetchData()}
                                refreshing={this.state.isfetching}
                            /> */}

                        </View>
                    </View>
                       
                </Content>
            </Container>
        );
    }
}
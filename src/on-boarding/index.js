import 
        React, 
        { 
            Component 
        } from 'react';
        
import { 
            View, 
            ImageBackground, 
            StyleSheet, 
            Platform, 
            YellowBox, 
            Image, 
            Text, 
            StatusBar 
        } from 'react-native';

import Onboarding from 'react-native-onboarding-swiper';


export default class OnBoarding extends Component
{   
    constructor(props)
    {
        super(props);

        YellowBox.ignoreWarnings([
            'Warning : componentWillMount is deprecated',
            'warning : componentWillReceiveProps is deprecated',
        ]);
    }

    render()
    {
        const skip = () =>
        {
            this.props.navigation.navigate('LoginPage');
        }

        return(
            <Onboarding
                onSkip = { skip }
                onDone = { skip }
                pages={[
                    {
                        backgroundColor: '#fff',
                        image: <Image style={{width:200, height:200, marginLeft:10, marginRight:10 }} source={require('../../assets/onBoardingA2x.png')} />,
                        title: 'Schedulle',
                        subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sodales gravida massa, ac porttitor erat tempor vel. Suspendisse imperdiet rutrum rhoncus. ',
                    },
                    {
                        backgroundColor: '#fff',
                        image: <Image style={{width:200, height:200, marginLeft:10, marginRight:10 }} source={require('../../assets/onBoardingB2x.png')} />,
                        title: 'Task List',
                        subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sodales gravida massa, ac porttitor erat tempor vel. Suspendisse imperdiet rutrum rhoncus. ',
                    },
                    {
                        backgroundColor: '#fff',
                        image: <Image style={{width:200, height:200, marginLeft:10, marginRight:10 }} source={require('../../assets/onBoardingC2x.png')} />,
                        title: 'Leave',
                        subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sodales gravida massa, ac porttitor erat tempor vel. Suspendisse imperdiet rutrum rhoncus. ',
                    }
                ]}
                />
        );
    }
}
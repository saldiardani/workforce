import React, { Component } from 'react';
import { AsyncStorage, View, Text, YellowBox, StatusBar, Image, NetInfo,ImageBackground} from 'react-native';
import { Button, Form, Item, Input, Label, Icon } from 'native-base';

import styles from './style';
import Network from '../../config/network';
import Endpoint from '../../config/endpoint';
import Utility from '../../config/utility';

import { TextLoader, LinesLoader, RippleLoader  } from 'react-native-indicator';
import Storage from 'react-native-storage';
//import SQLite from 'react-native-sqlite-2';


let storage = new Storage({
    size: 1000,
    storageBackend: AsyncStorage,
    defaultExpires: 1000 * 3600 * 30,
    enableCache: true,
    sync : {}
});

export default class LoginPage extends Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            username    : '',
            password    : '',
            wrongpass   : false,
            employeeID  : null,
            isConnected : false,
            isloaded    : true
        }

        Utility.removeYellowWarning();
    }

    fetchDataLocally = () =>
    {
        storage.load
        ({
            key: 'loginState',
            autoSync: true,
            syncInBackground: true,
            syncParams: 
            {
              extraFetchOptions: 
              {
                // blahblah
              },
              someFlag: true,
            },
        })
        
        .then(ret => 
        {
            console.log('2');
                console.log(ret.userid);
                console.log(ret.username);
                console.log(ret.token);
        
                let username = ret.username;
                let userid = ret.userid;
                let token = ret.token;
        
                if(username !='' && userid != '')
                {
                    console.log('3');
                    //this.setState({isloaded : false });
                    this.props.navigation.navigate('Dashboard');
                }
        })
        
        .catch(err => 
        {
            console.log('4');
                console.warn(err.message);
                switch (err.name) 
                {
                    case 'NotFoundError':
                        // TODO;
                        break;
                    case 'ExpiredError':
                        storage.clearMapForKey('loginState');
                        this.setState({isloaded : false });

                        console.log(this.state.isloaded);
                        break;
                }
        });

        console.log('5');
        setTimeout(() => this.setState({isloaded:false}), 15000);
    }

    wrongPass = () =>
    {
        this.setState({wrongpass:true});
        setTimeout(() => this.setState({wrongpass:false}), 10000);
    }

    onLoginButton = () =>
    {
        var username = this.state.username;
        var password = this.state.password;

        if(username=="" || password=="")
        {
            this.wrongPass();
        }
        else
        {
            //let EndPoint = 'http://172.25.230.165/identity/user/login';
            let EndPoint = 'http://ads-api.asyst.co.id/identity/user/login'; 

            Network.reqcheckauth(EndPoint,username,password)
            .then( (response) => 
            {
                console.log(response);

                if(response.status==='Success')
                {
                    let employeeID = response.employeeId;
                    let username = response.username;
                    let roleDesc = response.roleDesc;

                     
                    storage.save({
                        key: 'loginState', 
                        data: { 
                            username: username,
                            userid: employeeID,
                            token: '',
                            roleDesc : roleDesc
                        },
                    
                        expires: 1000 * 3600 * 36
                    });

                    this.props.navigation.navigate('Dashboard');
                }
                else
                {
                    this.wrongPass();
                }   
            });
        }
    }

    componentDidMount()
    {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount() 
    {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    handleConnectivityChange = isConnected => 
    {
        console.log(isConnected)
        if (isConnected) 
        {
            console.log(isConnected);
            this.setState({ isConnected });
        } 
        else 
        {
            console.warn(isConnected);
            this.setState({ isConnected });
        }
    };

    
    render()
    {
        if(this.state.isloaded)
        {
            this.fetchDataLocally();

            return(

                <View style={styles.container}>
                    <StatusBar backgroundColor="#0288D1" barStyle = "light-content" hidden = {false} />
                    <View style={{flex:1, flexDirection:'column', justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center'}}>                          
                    <RippleLoader size={100} />

                    <Text style={{ fontSize:25, fontWeight:'400', marginTop:80 }}>Please wait</Text>
                    <TextLoader text="checking user login on current device" textStyle={{marginTop:0, fontSize:15}}/>  
                           
                    </View>
                </View>
                
            );            
        }
        else
        {
            return(
                <View style={styles.container}>
                    <StatusBar backgroundColor="#0288D1" barStyle = "light-content" hidden = {false} />
                    <View style={styles.container}>
                        <View style={styles.logoContainer}>
                            <View style={styles.logoPositionContainer} >
                                <Image source={require('../../assets/logo-blackText.png')} style={styles.logo} />
                            </View>
                            {
                                ( this.state.wrongpass ) 
                                 ?  
                                 <View style={styles.wrongPassContainer}>
                                     <Text style={styles.wrongPassText}> Oopss..! your username or passoword is not match. {"\n"} Please check your typing. </Text>   
                                 </View>
                                 :
                                     null
                             }     
    
                             <View style={styles.formContainer}>
                                 <Form>
                                     <Item style={{borderBottomColor:'transparent'}}>
                                     <Icon active name='ios-person-outline' style={styles.iconTextField} />
                                         <Input  placeholder='Username' 
                                                 tintColor={'#9E9E9E'} 
                                                placeholderTextColor={'#BDBDBD'} 
                                             underlineColorAndroid="#BDBDBD" 
                                                style={styles.inputField}
                                                onChangeText = {(value)=>{this.setState({username:value.toLowerCase()})}}
                                                 returnKeyType={ "next" }
                                             onSubmitEditing={(event) => {
                                                     this.refs['Password']._root.focus()} }
                                                 />
                                    </Item>
        
                                     <Item style={{marginTop:20, borderBottomColor:'transparent'}}>
                                         <Icon a ctive name='ios-unlock-outline' style={{color:'#616161', fontSize:25}}/>
                                         <Input  placeholder='Password'
                                                 ref='Password' 
                                             secureTextEntry={true} 
                                                 placeholderTextColor={'#BDBDBD'} 
                                                underlineColorAndroid="#BDBDBD" 
                                                 style={{color:'#212121', fontSize:20}}
                                                 onChangeText = {(value)=>{this.setState({password:value.toLowerCase()})}}
                                                 returnKeyType={ "done" }
                                                 />
                                     </Item>
        
                                 </Form>
                             </View>
                        </View>

                         <View style={{flex:6, flexDirection:'column', marginTop:0, marginBottom:0, bottom:0}}>
                             <ImageBackground source = {require('../../assets/sky2x.png')} style={{flex:1, height:200, bottom:0}}>
                                 <View style={{flex:0.5, flexDirection:'column', justifyContent:'flex-start', alignContent:'flex-start', alignItems:'flex-start'}}>
                                     <Button block rounded style={{backgroundColor:'#0288D1', marginLeft:60, marginRight:60, height:50, elevation:0}} onPress={this.onLoginButton}>
                                         <Text style={{ color:'#fff',fontSize:18, fontWeight:'bold'}}> Login </Text>
                                     </Button>
                                 </View>

                                 <View style={{flex:1, flexDirection:'column', justifyContent:'flex-start', alignContent:'flex-start'}}>
                                     <Button block transparent style={{marginLeft:50, marginRight:50, elevation:0}}>
                                         <Text style={{ flex:0.5, color:'#212121', fontSize:13, textAlign:'center'}}> Forgot Password ? </Text>
                                     </Button>   
                                 </View> 
                             </ImageBackground>                        
                         </View>

                         </View>
                </View>
            );
        }
    }
}
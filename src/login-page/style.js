import { StyleSheet } from 
'react-native';

export default StyleSheet.create
({
    container:
    {
        flex:1, 
        flexDirection:'column', 
        backgroundColor:'#FAFAFA'
    },

    logoContainer :
    {
        flex:15
    },

    logoPositionContainer :
    {
        flex:10, 
        justifyContent:'center', 
        alignContent:'center', 
        alignItems:'center', 
        marginTop:0
    },

    logo :
    {
        flex:1, 
        aspectRatio: 1.6, 
        resizeMode: 'contain', 
        width:400, 
        height:200
    },

    wrongPassContainer:
    {
        flex:1, 
        position:'absolute', 
        left:0, 
        right:0, 
        bottom: 200, 
        flexDirection:'column',
        flexDirection:'column', 
        justifyContent:'flex-start', alignContent:'flex-start', 
        alignItems:'center', 
        marginLeft:20, 
        marginRight:20
    },

    wrongPassText:
    {
        color:'#0277BD', 
        fontSize:15, 
        textAlign:'center'
    },

    formContainer :
    {
        flex:13,
        flexDirection:'column',
        justifyContent:'center', 
        alignContent:'center', 
        marginLeft:20, 
        marginRight:20
    },

    iconTextField :
    {
        color:'#616161', 
        fontSize:25
    },

    inputField :
    {
        color:'#212121', 
        fontSize:20
    }


});
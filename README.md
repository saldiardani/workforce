Additional Plugin and UI Frameworks : 

Nativabase : https://docs.nativebase.io/docs/GetStarted.html


Element : https://docs.nativebase.io/docs/GetStarted.html


Navigation : https://github.com/react-navigation/react-navigation


Carousel : https://github.com/chilijung/react-native-carousel-view


Dropdown : https://github.com/n4kz/react-native-material-dropdown#readme


Radio Button :  https://github.com/thegamenicorus/react-native-flexi-radio-button


Sound : https://github.com/zmxv/react-native-sound


PopupMenu : https://github.com/instea/react-native-popup-menu


Modal : https://github.com/react-native-community/react-native-modal


ModalBox : https://github.com/maxs15/react-native-modalbox#readme


On Boarding : https://github.com/jfilter/react-native-onboarding-swiper#readme


Moment : https://github.com/headzoo/react-moment#readme


Dash Line : https://github.com/obipawan/react-native-dash#readme
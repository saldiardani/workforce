import { YellowBox } from 'react-native';

let getCurrentDay = () =>
{
    return currentDay = (new Date()).getDate();
}

let getCurrentMonth = () =>
{
    return currentMonth = (new Date()).getMonth()+1;
}

let getCurrentYear = () =>
{
    return currentYear = (new Date()).getFullYear();
}

let getCurrentDate = () =>
{
    var dateArray = [];

    var dd      = getCurrentDay();
    var mm      = getCurrentMonth();
    var yyyy    = getCurrentYear();

    return dateArray = [dd, mm, yyyy];
}


let getLastDayOfMonth = () =>
{
    var dateArray = getCurrentDate();
    var lastDay   = (new Date(dateArray[1], dateArray[2]+1, 0)).getDate();

    return getFormatDay(lastDay,dateArray[1],dateArray[2],5);
}

let getMonthString = ( number ) =>
{
    //let number = number;
    let string = null;

    switch(number)
    {
        case 1 :
            string = 'Jan';
            break;
        
        case 2 :
            string = 'Feb';
            break;

        case 3 :
            string = 'Mar';
            break;
        
        case 4 :
            string = 'Apr';
            break;

        case 5 :
            string = 'Mei';
            break;

        case 6 :
            string = 'Jun';
            break;

        case 7 :
            string = 'Jul';
            break;

        case 8 :
            string = 'Aug';
            break;

        case 9 :
            string = 'Sep';
            break;

        case 10 :
            string = 'Oct';
            break;

        case 11 :
            string = 'Nov';
            break;

        case 12 :
            string = 'Dec';
            break;
        default :
            break;
    }

    return string;
}

let getFormatDay = (day,month,year,mode) =>
{
    var format  = '';
    var day     = day<10 ? '0'+day : day;
    var month   = month<10 ? '0'+month : month;
    var year    = year;
    var mode    = mode;

    switch(mode) 
    {
        case 0:
            format = month + '/' + day + '/' + year;
            break;
        
        case 1:
            format = day + '/' + month + '/' + year;
            break;
        
        case 2:
            format = year + '/' + month + '/' + day ;
            break;
        
        case 3:
            format = month + '-' + day + '-' + year;
            break;
        
        case 4:
            format = day + '-' + month + '-' + year;
            break;
        
        case 5:
            format = year + '-' + month + '-' + day ;
            break;

        default:
            break;
    }

    return format;
}


let getMinDateOfMonth = () =>
{
    var dateArray   = getCurrentDate();
    var minDate     = dateArray[0]>1 ? dateArray[0] - (dateArray[0]-1) : dateArray[0];

    return "'"+getFormatDay(minDate,dateArray[1],dateArray[2],5)+"'";
}

let getRandomColor = () =>
{
   return  '#'+(Math.random()*0xFFFFFF<<0).toString(16);
}

let removeYellowWarning = () =>
{
    YellowBox.ignoreWarnings([
        'Warning: componentWillMount is deprecated',
        'Warning: componentWillReceiveProps is deprecated',
        'Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'
    ]);

    console.disableYellowBox = true;
}

let getFirstAlphabentEachWord = (username) =>
{
    let spaceIndex  = username.indexOf('.');
    let firstWord   = username.substring(0, spaceIndex);
    let lastWord    = username.substring(spaceIndex+1);

    return (firstWord.substring(0,1)).toUpperCase() + (lastWord.substring(0,1)).toUpperCase();
}

let getUsername = (username) =>
{
    let spaceIndex  = username.indexOf('.');
    let firstWord   = username.substring(0, spaceIndex);
    let lastWord    = username.substring(spaceIndex+1);

    return firstWord.toUpperCase() + ' ' + lastWord.toUpperCase(); 
}

let getHourMinute = (time) =>
{
    let hour_minute = time.substring(0,5);

    return hour_minute;
}

export default 
{ 
    removeYellowWarning,
    getCurrentDay,
    getCurrentDate, 
    getCurrentMonth,
    getMonthString, 
    getCurrentYear, 
    getLastDayOfMonth, 
    getFormatDay, 
    getMinDateOfMonth, 
    getRandomColor,
    getFirstAlphabentEachWord,
    getUsername,
    getHourMinute
};
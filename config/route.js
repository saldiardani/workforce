import React from 'react';
import {StackNavigator} from 'react-navigation';

/**Import Page */
import SplashScreen from '../src/splash-screen/index.js';
import OnBoarding from '../src/on-boarding/index.js';
import LoginPage from '../src/login-page/index.js';
import Dashboard from '../src/dashboard/index.js';
import TaskListDetail from '../src/task-list-detail/index.js';

export default StackNavigator(
  {
    OnBoarding    : { screen: OnBoarding },
    SplashScreen  : { screen: SplashScreen },
    LoginPage     : { screen: LoginPage },
    Dashboard     : { screen: Dashboard },
    TaskListDetail: { screen: TaskListDetail }
  },
  {
    initialRouteName : 'LoginPage',
    headerMode: 'none',
  }
);

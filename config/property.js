//Property
let HeaderTitle     = '';
let SubHeaderTitle  = '';


//Base Path Image :
let BasePathImage   = '../../assets/';


//Header Title Configuration
let setHeaderTitle = (title) =>
{
    this.HeaderTitle = title;
}

//Get Header Title
let getHeaderTitle = () => 
{
    return this.HeaderTitle;
}

// Sub Header Title Configuration
let setSubHeaderTitle = (title) =>
{
    this.SubHeaderTitle = title;
}

//Get SubHeader Title
let getSubHeaderTitle = () =>
{
    return this.SubHeaderTitle;
}

//Export these Functions
export default {setHeaderTitle, getHeaderTitle, setSubHeaderTitle, getSubHeaderTitle}
global.APIEndPoint  = '';
global.Response     = '';

//Ckear endpoint
let clearEndPoint = () =>
{
    APIEndPoint = '';
}

//Clear response
let clearResponse = () =>
{
    Response = '';
}

//get all user login :
let reqgetuserlogin = (url) =>
{
    clearEndPoint();
    clearResponse();

    APIEndPoint = url;


    return fetch(APIEndPoint,
    {
        method: "GET",
        headers: 
        {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    })
    .then((response) => response.json())
    .then((responseData) => {
    //   console.warn(responseData);
      return responseData;
    })
    .catch(error => console.warn(error));
}


//get authentification :
let reqcheckauth = (url, username, password) =>
{
    clearEndPoint();
    clearResponse();

    APIEndPoint     = url;
    var username    = username;
    var password    = password;

    return fetch(APIEndPoint,
        {
            method  : "POST",
            headers :
            {
                'Accept'        : 'application/json',
                'Content-Type'  : 'application/json'
            },
            body    : JSON.stringify
                        ({
                            username    : username,
                            password    : password
                        })
        })
        .then((response) => response.json())
        .then((responseData) => {
           //console.warn(responseData);
          return responseData;
        })
        .catch(error => console.warn(error));
}

//get all list data dashboard
let reqdatalistdashboard = (url) => 
{
    return fetch(url,
        {
            method  : "GET",
            headers :
            {
                'Accept'        : 'application/json',
                'Content-Type'  : 'application/json'
            }
        })
        .then((response) => response.json())
        .then((responseData) => {
           //console.warn(responseData);
          return responseData;
        })
        .catch(error => console.warn(error));
}

//get all tasklist data : 
let reqdatatasklistdetail = (url) =>
{
    return fetch(url,
        {
            method  : "GET",
            headers :
            {
                'Accept'        : 'application/json',
                'Content-Type'  : 'application/json'
            }
        })
        .then((response) => response.json())
        .then((responseData) => {
           //console.warn(responseData);
          return responseData;
        })
        .catch(error => console.warn(error));
}


let reqstarttask = (url) =>
{
    return fetch(url,
        {
            method  : "POST",
            headers :
            {
                'Accept'        : 'application/json',
                'Content-Type'  : 'application/json'
            }
        })
        .then( (response)=> response.json() )
        .then((responseData) => {
            return responseData;
        })
        .catch(error => console.warn(error));
}

export default 
{ 
    reqgetuserlogin, 
    reqcheckauth, 
    reqdatalistdashboard,
    reqdatatasklistdetail,
    reqstarttask
};
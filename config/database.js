import React, {Component} from 'react';
import SQLite from 'react-native-sqlite-2';

const DB_NAME = 'workforce.db';

const db = '';

let connectionpreparation = () =>
{
    db = SQLite.openDatabase(DB_NAME, '1.0', '', 1);
}

let createtable = () =>
{
    db.transaction(function(txn)
    {
        txn.executeSql('CREATE TABLE IF NOT EXISTS Users(user_id INTEGER PRIMARY KEY NOT NULL, ID integer)', []);
    });
}

let droptableifexists = () =>
{
    db.transaction(function(txn)
    {
        txn.executeSql('DROP TABLE IF EXISTS Users', []);
    });
}

let insertvaluesingle = (table_name, value) =>
{
    db.transaction(function(txn)
    {
        txn.executeSql('INSERT INTO Users ('+table_name+') VALUES (?)', [value]);
    });
}

let selectalldata = () =>
{
    result = '';

    db.transaction(function(txn)
    {
        txn.executeSql('SELECT * FROM `users`', [], function (tx, res) 
        {
            for (let i = 0; i < res.rows.length; ++i) 
            {
                console.log('item:', res.rows.item(i));
                result = res.rows.item(i);
            }
        });
    });

    return result;
}

export default 
{
    connectionpreparation,
    droptableifexists,
    createtable,
    insertvaluesingle,
    selectalldata
}